#include <stdio.h>
#include <math.h>

int main(){
	
	int l1,l2,l3,perimetro;
	
	printf("PERIMETRO TRIANGULO ISOCELES \n");
	
	printf("\n\nIngrese el primer lado: "); scanf("%d", &l1);
	
	printf("\n\nIngrese el segundo lado: "); scanf("%d", &l2);
	
	
	perimetro = (2*l1)+l2;
	
	printf("El perimetro del triangulo Isoceles es: %d", perimetro);
}

