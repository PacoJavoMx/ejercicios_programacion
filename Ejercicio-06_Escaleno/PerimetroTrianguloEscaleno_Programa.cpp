#include <stdio.h>
#include <math.h>

int main(){
	
	int l1,l2,l3,perimetro;
	
	printf("PERIMETRO TRIANGULO ESCALENO \n");
	
	printf("\n\nIngrese el primer lado: "); scanf("%d", &l1);
	
	printf("\n\nIngrese el segundo lado: "); scanf("%d", &l2);
	
	printf("\n\nIngrese el tercer lado: "); scanf("%d", &l3);
	
	perimetro = l1+l2+l3;
	
	printf("El perimetro del triangulo escaleno es: %d", perimetro);
}
