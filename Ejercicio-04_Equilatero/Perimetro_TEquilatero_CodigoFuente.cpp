#include <stdio.h>
#include <math.h>

int main(){
	
	int l1, perimetro;
	
	printf("PERIMETRO TRIANGULO EQUILATERO \n");
	
	printf("\n\nIngrese el  lado: "); scanf("%d", &l1);
	
	perimetro = l1*3;
	
	printf("El perimetro del triangulo equilatero es: %d", perimetro);
}
